﻿
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace LoggerService.Contracts
{
   public interface ILoggerManager
    {
        void Debug(string message, object arg1, object arg2);
        void Debug(string message, object arg1, object arg2, object arg3);
        void Debug(Exception exception, [Localizable(false)] string message);
        void Debug([Localizable(false)] string message);
        void Debug([Localizable(false)] string message, params object[] args);
        void Debug([Localizable(false)] string message, Exception exception);
        void DebugException([Localizable(false)] string message, Exception exception);
        void Error(object value);
        void Error(string message, object arg1, object arg2);
        void Error(string message, object arg1, object arg2, object arg3);
        void Error(Exception exception, [Localizable(false)] string message);
        void Error(Exception exception, [Localizable(false)] string message, params object[] args);
        void Error([Localizable(false)] string message);
        void Error([Localizable(false)] string message, params object[] args);
        void Error([Localizable(false)] string message, Exception exception);
        void ErrorException([Localizable(false)] string message, Exception exception);
        void Info(object value);
        void Info(string message, object arg1, object arg2);
        void Info(string message, object arg1, object arg2, object arg3);
        void Info(string message, object argument);
        void Info<T>(T value);
        void Info(Exception exception, [Localizable(false)] string message);
        void Info(Exception exception, [Localizable(false)] string message, params object[] args);
        void Info([Localizable(false)] string message);
        void Info([Localizable(false)] string message, params object[] args);
        void Info([Localizable(false)] string message, Exception exception);
        void Log(LogLevel level, object value);
        void Log(LogLevel level, string message, object arg1, object arg2);
        void Log(LogLevel level, string message, object arg1, object arg2, object arg3);
        void Log(LogLevel level, string message, string argument);
        void Log(LogLevel level, string message, object argument);
        void Log<T>(LogLevel level, T value);
        void Log(LogLevel level, Exception exception, [Localizable(false)] string message, params object[] args);
        void Log(LogLevel level, [Localizable(false)] string message);
        void Log(LogLevel level, [Localizable(false)] string message, params object[] args);
        void Log(LogLevel level, [Localizable(false)] string message, Exception exception);
        void Trace(object value);
        void Trace(string message, object arg1, object arg2);
        void Trace(string message, object arg1, object arg2, object arg3);
        void Trace(string message, object argument);
        void Trace<T>(T value);
        void Trace(Exception exception, [Localizable(false)] string message);
        void Trace(Exception exception, [Localizable(false)] string message, params object[] args);
        void Trace([Localizable(false)] string message);
        void Trace([Localizable(false)] string message, params object[] args);
        void Trace([Localizable(false)] string message, Exception exception);
        void TraceException([Localizable(false)] string message, Exception exception);
        void Warn(object value);
        void Warn(string message, object arg1, object arg2);
        void Warn(string message, object arg1, object arg2, object arg3);
        void Warn(string message, object argument);
        void Warn<T>(T value);
        void Warn(Exception exception, [Localizable(false)] string message);
        void Warn(Exception exception, [Localizable(false)] string message, params object[] args);
        void Warn([Localizable(false)] string message);
        void Warn([Localizable(false)] string message, params object[] args);
        void Warn([Localizable(false)] string message, Exception exception);
    }
}




