﻿using LoggerService.Contracts;
using NLog;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace LoggerService
{
    public class LoggerManager : ILoggerManager
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        public LoggerManager()
        {
        }

        public void Debug(object value)
        {
            logger.Debug(value);
        }
        
        public void Debug(string message, object arg1, object arg2)
        {
            logger.Debug(message, arg1, arg2);
        }

        public void Debug(string message, object arg1, object arg2, object arg3)
        {
            logger.Debug(message, arg1, arg2,arg3);
        }
        
        public void Debug(Exception exception, [Localizable(false)] string message)
        {
            logger.Debug(exception, message);
        }

        public void Debug([Localizable(false)] string message)
        {
            logger.Debug(message);
        }

        public void Debug([Localizable(false)] string message, params object[] args)
        {
            logger.Debug(message,args);
        }
        [Obsolete]
        public void Debug([Localizable(false)] string message, Exception exception)
        {
            logger.Debug(message, exception);
        }
        [Obsolete]
        public void DebugException([Localizable(false)] string message, Exception exception)
        {
            logger.DebugException(message, exception);
        }

        public void Error(object value)
        {
            logger.Error(value);
        }
        
        public void Error(string message, object arg1, object arg2)
        {
            logger.Error(message, arg1, arg2);
        }

        public void Error(string message, object arg1, object arg2, object arg3)
        {
            logger.Error(message, arg1, arg2, arg3);
        }
        
        public void Error(Exception exception, [Localizable(false)] string message)
        {
            logger.Error(exception, message);
        }

        public void Error(Exception exception, [Localizable(false)] string message, params object[] args)
        {
            logger.Error(exception, message, args);
        }
        
        public void Error([Localizable(false)] string message)
        {
            logger.Error(message);
        }

        public void Error([Localizable(false)] string message, params object[] args)
        {
            logger.Error(message, args);
        }
        [Obsolete]
        public void Error([Localizable(false)] string message, Exception exception)
        {
            logger.Error(message, exception);
        }
        [Obsolete]
        public void ErrorException([Localizable(false)] string message, Exception exception)
        {
            logger.ErrorException(message, exception);
        }
        
        public void Info(object value)
        {
            logger.Info(value);
        }
        
        public void Info(string message, object arg1, object arg2)
        {
            logger.Info(message, arg1, arg2);
        }

        public void Info(string message, object arg1, object arg2, object arg3)
        {
            logger.Info(message, arg1, arg2, arg3);
        }

        public void Info(string message, object argument)
        {
            logger.Info(message, argument);
        }
        
        public void Info<T>(T value)
        {
            logger.Info(value);
        }
        
        public void Info(Exception exception, [Localizable(false)] string message)
        {
            logger.Info(exception, message);
        }

        public void Info(Exception exception, [Localizable(false)] string message, params object[] args)
        {
            logger.Info(exception, message, args);
        }
        
        public void Info([Localizable(false)] string message)
        {
            logger.Info(message);
        }

        public void Info([Localizable(false)] string message, params object[] args)
        {
            logger.Info(message, args);
        }
        [Obsolete]
        public void Info([Localizable(false)] string message, Exception exception)
        {
            logger.Info(message, exception);
        }
        
        public void Log(LogLevel level, object value)
        {
            logger.Log(level, value);
        }
        
        public void Log(LogLevel level, string message, object arg1, object arg2)
        {
            logger.Log(level, message, arg1, arg2);
        }

        public void Log(LogLevel level, string message, object arg1, object arg2, object arg3)
        {
            logger.Log(level, message, arg1, arg2, arg3);
        }
        
        public void Log(LogLevel level, string message, string argument)
        {
            logger.Log(level, message, argument);
        }
        
        public void Log(LogLevel level, string message, object argument)
        {
            logger.Log(level, message, argument);
        }
        
        public void Log(LogEventInfo logEvent)
        {
            logger.Log(logEvent);
        }

        public void Log(Type wrapperType, LogEventInfo logEvent)
        {
            logger.Log(wrapperType, logEvent);
        }

        public void Log<T>(LogLevel level, T value)
        {
            logger.Log(level, value);
        }
        
        public void Log(LogLevel level, Exception exception, [Localizable(false)] string message, params object[] args)
        {
            logger.Log(level, exception, message, args);
        }
        
        public void Log(LogLevel level, [Localizable(false)] string message)
        {
            logger.Log(level, message);
        }

        public void Log(LogLevel level, [Localizable(false)] string message, params object[] args)
        {
            logger.Log(level, message, args);
        }
        [Obsolete]
        public void Log(LogLevel level, [Localizable(false)] string message, Exception exception)
        {
            logger.Log(level, message, exception);
        }
        
        public void Trace(object value)
        {
            logger.Trace(value);
        }
        
        public void Trace(string message, object arg1, object arg2)
        {
            logger.Trace(message, arg1, arg2);
        }

        public void Trace(string message, object arg1, object arg2, object arg3)
        {
            logger.Trace(message, arg1, arg2, arg3);
        }


        public void Trace(string message, object argument)
        {
            logger.Trace(message, argument);
        }


        public void Trace<T>(T value)
        {
            logger.Trace(value);
        }


        public void Trace(Exception exception, [Localizable(false)] string message)
        {
            logger.Trace(exception, message);
        }

        public void Trace(Exception exception, [Localizable(false)] string message, params object[] args)
        {
            logger.Trace(exception, message, args);
        }


        public void Trace([Localizable(false)] string message)
        {
            logger.Trace(message);
        }

        public void Trace([Localizable(false)] string message, params object[] args)
        {
            logger.Trace(message,args);
        }
        [Obsolete]
        public void Trace([Localizable(false)] string message, Exception exception)
        {
            logger.Trace(message, exception);
        }
        [Obsolete]
        public void TraceException([Localizable(false)] string message, Exception exception)
        {
            logger.TraceException(message, exception);
        }

        public void Warn(object value)
        {
            logger.Warn(value);
        }


        public void Warn(string message, object arg1, object arg2)
        {
            logger.Warn(message, arg1, arg2);
        }

        public void Warn(string message, object arg1, object arg2, object arg3)
        {
            logger.Warn(message, arg1, arg2, arg3);
        }


        public void Warn(string message, object argument)
        {
            logger.Warn(message, argument);
        }

        public void Warn<T>(T value)
        {
            logger.Warn(value);
        }

        public void Warn(Exception exception, [Localizable(false)] string message)
        {
            logger.Warn(exception, message);
        }

        public void Warn(Exception exception, [Localizable(false)] string message, params object[] args)
        {
            logger.Warn(exception, message,args);
        }

        public void Warn([Localizable(false)] string message)
        {
            logger.Warn(message);
        }

        public void Warn([Localizable(false)] string message, params object[] args)
        {
            logger.Warn(message,args);
        }
        [Obsolete]
        public void Warn([Localizable(false)] string message, Exception exception)
        {
            logger.Warn(message,exception);
        }

    }
}
