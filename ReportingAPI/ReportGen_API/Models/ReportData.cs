﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportGen_API.Models
{
    public class ReportData
    {
        public string ProjectName { get; set; }
        public string Date { get; set; }
        public int TicketProcessed { get; set; }
        public int Success { get; set; }
        public int Failure { get; set; }
        public string Direction { get; set; }
    }
}
