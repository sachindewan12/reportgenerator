﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using LoggerService.Contracts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using ReportGen_API.Models;

namespace ReportGen_API.Controllers
{
    [Route("api/[controller]")]
    public class FileUploadController : Controller
    {
        public IHostingEnvironment HostingEnvironment { get; }
        public IFileProvider FileProvider { get; }
        public ILoggerManager LoggerManager { get; set; }

        public FileUploadController(IHostingEnvironment hostingEnvironment, IFileProvider fileProvider, ILoggerManager _loggerManager)
        {
            LoggerManager = _loggerManager;
            FileProvider = fileProvider;
        }
        [HttpPost("upload")]
        public async Task<IActionResult> UploadAsync(IFormFile file)
        {
            //var file = Request.Form.Files[0];
            XDocument xDoc = XDocument.Load(file.OpenReadStream()); 
            LoggerManager.Debug(file.FileName);
            if (file == null || file.Length == 0)
                return Content("file not selected");

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot",
                        file.FileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return StatusCode(StatusCodes.Status200OK, GetReportData());
        }
        [HttpPost("generateReport")]
        public  IActionResult GenerateReport(IFormFile file)
        {
            LoggerManager.Debug(file.FileName);
            if (file == null || file.Length == 0)
                return Content("file not selected");
            XDocument xDoc = XDocument.Load(file?.OpenReadStream());
            var lstReportData = new List<ReportData>();
            // XmlNodeList xList = xDoc.SelectNodes("/tickets/ticket");
            var expr = from d in xDoc.Descendants("ticket")
                       group d by new
                       {
                           ProjectName = d.Element("Project").Value,
                           Date = d.Element("Date").Value,
                           Direction = d.Element("Direction").Value,

                       } into gDir
                       select new
                       {
                           GroupKey = gDir.Key,
                           Status = from z in gDir
                                    select new { Value = z.Element("Status").Value },

                       };
            foreach (var g in expr)
            {
                Console.WriteLine("{0}\t{1}\t\t{2}", g.GroupKey.ProjectName, g.GroupKey.Date, g.GroupKey.Direction);
                int Success = 0;
                int failure = 0;
                int Tickets_Processed = 0;
                foreach (var n in g.Status)
                {
                    if (Convert.ToInt32(n.Value.ToString()) == 1)
                    {
                        Success += 1;
                    }
                    else
                    {
                        failure += 1;
                    }
                    Tickets_Processed += 1;
                }
                lstReportData.Add(new ReportData { Date = g.GroupKey.Date, ProjectName = g.GroupKey.ProjectName, Direction = g.GroupKey.Direction, TicketProcessed = Tickets_Processed, Success = Success, Failure = failure });
                Console.WriteLine("\t\t{0}\t{1}\t{2}", Tickets_Processed, Success, failure);

            }
            return  StatusCode(StatusCodes.Status200OK, lstReportData);
        }
        [HttpGet("download")]
        public IActionResult Download()
        {
            var imageUrl = new List<string>();
            var directoryContent = FileProvider.GetDirectoryContents(string.Empty);
            var content = directoryContent?.ToList();
            content?.ForEach(data =>
            {

                imageUrl.Add(HostingEnvironment.IsDevelopment() ? "http://localhost:55904/" + data.Name : "http://riyanwebapi.azurewebsites.net/" + data.Name);
            });
            if (imageUrl.Count > 0)
            {
                return Ok(new { images = imageUrl });
            }

            return NotFound(new { message = "image not found" });

        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }
        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        private List<ReportData> GetReportData()
        {
            var lstReportData = new List<ReportData>();
            var directoryContent = FileProvider.GetDirectoryContents(string.Empty);
            var content = directoryContent?.ToList();
            XDocument xDoc = XDocument.Load(content[0]?.PhysicalPath);
            FileInfo file = new FileInfo(content[0]?.PhysicalPath);
            if (file.Exists)
            {
                file.Delete();
            }
            // XmlNodeList xList = xDoc.SelectNodes("/tickets/ticket");
            var expr = from d in xDoc.Descendants("ticket")
                       group d by new
                       {
                           ProjectName = d.Element("Project").Value,
                           Date = d.Element("Date").Value,
                           Direction = d.Element("Direction").Value,

                       } into gDir
                       select new
                       {
                           GroupKey = gDir.Key,
                           Status = from z in gDir
                                    select new { Value = z.Element("Status").Value },

                       };
            foreach (var g in expr)
            {
                Console.WriteLine("{0}\t{1}\t\t{2}", g.GroupKey.ProjectName, g.GroupKey.Date, g.GroupKey.Direction);
                int Success = 0;
                int failure = 0;
                int Tickets_Processed = 0;
                foreach (var n in g.Status)
                {
                    if (Convert.ToInt32(n.Value.ToString()) == 1)
                    {
                        Success += 1;
                    }
                    else
                    {
                        failure += 1;
                    }
                    Tickets_Processed += 1;
                }
                lstReportData.Add(new ReportData { Date = g.GroupKey.Date, ProjectName = g.GroupKey.ProjectName, Direction = g.GroupKey.Direction, TicketProcessed = Tickets_Processed, Success = Success, Failure = failure });
                Console.WriteLine("\t\t{0}\t{1}\t{2}", Tickets_Processed, Success, failure);

            }
            return lstReportData;
        }
    }
}