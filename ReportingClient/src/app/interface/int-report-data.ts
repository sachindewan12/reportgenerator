export interface IReportData {
    ProjectName: string;
    Date: string;
    TicketProcessed: number;
    Success: number;
    Failure: number;
    Direction: string;
}