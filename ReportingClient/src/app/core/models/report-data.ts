import { IReportData } from "../../interface/int-report-data";

export class ReportData implements IReportData {
    public Direction: string;
    public ProjectName: string;
    public Date: string;
    public TicketProcessed: number;
    public Success: number;
    public Failure: number;
}