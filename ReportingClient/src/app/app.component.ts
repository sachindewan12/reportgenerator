import { Component, ViewChild } from '@angular/core';
//import { FileUploader, Headers } from '../../node_modules/ng2-file-upload';
import { environment } from '../environments/environment';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'
import { FileuplodService } from './services/fileuplod.service';
import { ReportData } from './core/models/report-data';
import { IReportData } from './interface/int-report-data';
import { Table } from '../../node_modules/primeng/components/table/table';

const URL = environment.URL;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  multiSortMeta: any = [];
  @ViewChild("dt") dataTable: Table;
  @ViewChild("file") fileControl: any;
  rowHeader: any = [
    { field: 'projectName', header: 'Project Name' },
    { field: 'date', header: 'Date' },
    { field: 'ticketProcessed', header: 'Tickets Processed' },
    { field: 'success', header: 'Success' },
    { field: 'failure', header: 'Failure' },
    { field: 'direction', header: 'Direction' }
  ]
  selectedReportData: IReportData;
  reportData: IReportData[] = [];
  title = 'Monthly Report';
  public progress: any;
  public message: string;
  public invalidFile: string;
  constructor(private http: HttpClient, private fileuploader: FileuplodService) {

  }


  ngOnInit() {
    this.multiSortMeta.push({ field: 'projectName', order: 1 }, { field: 'direction', order: 1 });
  }

  upload(files) {
    this.message = "";
    this.invalidFile = "";
    if (files.length === 0)
      return;

    const formData = new FormData();
    let input = new FormData();
    input.append("file", files[0]);
    this.progress = true;
    return this.fileuploader.fileUpload(input)
      .subscribe((res: IReportData[]) => {
        debugger;
        this.progress = false;
        this.message = "file uploaded";
        this.dataTable.first = 0;
        this.reportData = res;
      }, error => {
        this.progress = false;
        this.message = "";
        this.reportData = [];
        this.invalidFile = JSON.stringify(error);
      });
  }

  onRowSelect(event) {
    this.selectedReportData = event.data;
  }

  refreshPage(data: any) {
    debugger;
    data.currentTarget.children[0].classList.add("fa-spin");
    this.reportData = [];
    this.message = "";
    this.invalidFile = "";
    this.fileControl.nativeElement.value = "";
    data.currentTarget.children[0].classList.remove("fa-spin");
  }
}
