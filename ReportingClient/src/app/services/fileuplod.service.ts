import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from '../../../node_modules/rxjs';
@Injectable({
  providedIn: 'root'
})
export class FileuplodService {

  constructor(private http: HttpClient) { }

  public fileUpload(data: any): Observable<any> {
    return this.http.post(environment.URL, data).pipe((map(res => {
      return res;
    })))
  }
}
